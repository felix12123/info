# Support

Neben der Hilfe, in welcher du dich gerade befindest, kannst du Fragen auf unserem Discord Server stellen.
Informationen, wie Discord funktioniert und wo du unseren Discord Server findest, gibt es [hier](discord.md).

## Supportsystem
Wir haben uns entschieden, den Support für dich so einfach wie möglich zu gestalten. Deswegen läuft es ganz klassisch per Mail.

`support@gigrawars.com`

Um uns die Arbeit zu erleichtern, schreibt bitte immer mit derselben Mail, welche du auch in deinem Spielaccount verwendest.

Das System wird dich immer über neue Aktivitäten in deinem Ticket informieren und du kannst nach dem Erhalt der Mails immer wieder auf die Mails antworten.
