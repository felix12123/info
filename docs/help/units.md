# Einheiten

## Schakal

**Synonym:** Schak

## Recycler

**Synonym:** Rec

Der Recycler ist in der Lage bei einem Kampf zerstörte Schiffe zu recyceln, dabei bestimmt sich die Effektivität durch
die [Recycling-Technik](research.md#recycling-technik) Forschung.

## Spionagesonden

**Synonym:** Sonde, Spio

Spionagesonden werden, wie der Name vermuten lässt zur Spionage von fremden Planeten genutzt. Dabei können
sie Spionageberichte sowohl mit dem Befehl Erkunden, als auch mit dem Befehl Angriff erstellen.

Die Sonden können auch für Sondenraids genutzt werden.

### Sondenraids

Bei Sondenraids handelt es sich um gezielte Angriffe nur mit Sonden. In GigraWars haben Sonden, anders als bei anderen
Spielen, einen Angriffswert und können deswegen nicht nur zur Erkundung genutzt werden.

Für einen Sondenraid werden je nach Verteidigung min. 2.000 Sonden benötigt. Der Vorteil von Sondenraids ist, die
Geschwindigkeit mit der zugeschlagen werden kann, ein Nachteil sind die hohen Treibstoffkosten.

## Renegade

**Synonym:** Rene

## Raider

**Synonym:** 

## Falcon

**Synonym:** Tarnbomber

## Kolonisationsschiff

**Synonym:** Kolo

## Tjuger

**Synonym:**

## Cougar

**Synonym:** Coug

## Longeagle V

**Synonym:** Lev, LeV

## Kleines Handelsschiff

**Synonym:** Händler, kl. Händler, KH

## Großes Handelsschiff

**Synonym:** gr. Händler, GH

## Noah

## Longeagle X

**Synonym:** Lex, LeX
