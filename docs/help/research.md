# Forschung

Die Forschungsseite bei Gigrawars bietet Spielern die Möglichkeit, ihre Schiffsflotte zu verbessern. Hier stehen drei
Arten von Forschung zur Verfügung: Antriebsforschung, Waffenforschung und Schiffsforschung.

## Antriebsforschung

Durch die Antriebsforschungen können Spieler die Treibstoffkosten ihrer Schiffe mit jeder Forschungsstufe reduzieren und
gleichzeitig die Geschwindigkeit des jeweiligen Schiffstyps erhöhen. Dies ermöglicht effizientere Reisen und schnellere
Reaktionen im Spiel.

## Waffenforschung

Waffenforschungen hingegen dienen dazu, die Angriffsstärke der Schiffe zu verstärken. Es wird empfohlen, alle drei
Forschungsbereiche gleichmäßig zu entwickeln oder in einem Verhältnis, das sich nach den Forschungskosten richtet,
auszubauen. Dadurch kann eine ausgewogene und effektive Flotte aufgebaut werden, die in verschiedenen Situationen
erfolgreich agieren kann.

## Schiffsforschung

Die Schiffsforschungen auf der Forschungsseite zielen darauf ab, die unterschiedlichen Eigenschaften der Schiffe zu
verbessern. Dies umfasst die erweiterte Schiffspanzerung (erhöht den Verteidigungswert) oder erhöhte Ladekapazität der
Schiffe. Spieler haben die Möglichkeit, ihre Flotte durch diese Forschungen an ihre bevorzugte Spielstrategie anzupassen
und ihre Schiffe gezielt zu stärken. Empfehlenswert ist ein höherer Ausbau der Schiffspanzerung, da es die einzige
Forschung zur Steigerung des Verteidigungswerts ist, wohingegen es drei Forschungstypen zur Steigerung des Angriffswerts
gibt (ein ausgewogener Angriffs- und Verteidigungswert ist am effizientesten).

### Recycling-Technik

Die Stufe der Recycling-Technik bestimmt darüber wie effektiv die [Recycler](units.md#recycler) beim Kampf die Rohstoffe
der zerstörten Schiffe verwerten können. Je aggressiver der Spielstil, umso mehr lohnt sich der Ausbau der
Recycling-Technik. 

**Hinweis zum Recyclen:** Beim Recyclen im Angriff werden maximal so viele Rohstoffe recycelt, wie die
Ladungskapazität der mitgeschickten Recycler hergibt. Die Ladekapazität der anderen Schiffstypen wird hierbei nicht
berücksichtigt. Beim Deffen hingegen reicht 1 Recycler. Die maximale Menge an recycelten Rohstoffen richtet sich hierbei
nach der Größe der Speicher des Planeten.

### Spionagetechnik

Die Spionagetechnik verbessert die Fähigkeit auf einen erfolgreichen Spionagebericht nur sehr marginal, weshalb sich ein
Ausbau nur bis Stufe 5 empfiehlt. Effizienter ist es, dass man für einen vollständigen Spionagebericht (Deep) die Anzahl
der Sonden bei der Erkundung erhöht.

## Voraussetzung

Für die Forschung auf einem Planeten wird ein Forschungszentrum mit der Mindeststufe 1 benötigt.
Weitere Stufen beschleunigen den Forschungsprozess.

## Paralleles forschen

Es besteht keine Möglichkeit auf mehreren Planeten gleichzeitig etwas zu erforschen, genauso ist es nicht möglich die
Gebäudestufen von mehreren Planeten zu bündeln.
