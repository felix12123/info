# Einstellungen

## Urlaubsmodus

Der Urlaubsmodus ermöglicht es, ähnlich wie im Arbeitsleben, ein paar Tage Auszeit vom Spiel zu nehmen.

### Wann kann ich in den Urlaubsmodus gehen?

Um in den Urlaubsmodus zu wechseln, darfst du keine eigenen Flotten mit dem Befehl Angriff oder Erkundung in der Luft
haben.

### Wie nutze ich den Urlaubsmodus?

Zum Aktivieren des Urlaubsmodus musst, du lediglich den Urlaubsmodus auf aktiv setzen und eine gewünschte Zahl an
Urlaubstagen auswählen. Dabei ist zu beachten, dass du jederzeit wieder aus dem Urlaubsmodus gehen kannst, aber nach der
gewählten Anzahl an Tagen automatisch herausfällst. Du bekommst ca. 24 Stunden vor Ablauf des Urlaubsmodus eine
entsprechende Mail als Erinnerung.

### Wie erkenne ich, ob ich im Urlaubsmodus bin?

Du siehst in den Einstellungen einen Text "Du bist noch bis zum ... im Urlaubsmodus."

### Was passiert mit dem Account während ich im Urlaubsmodus bin?

Du kannst nicht von anderen Spieler angeflogen werden, Ausnahme bilden hier schon fliegende Flotten. Alle deine
Bauschleifen werden eingefroren bis du wieder aus dem Urlaubsmodus rauskommst. Du kannst das Spiel weiterhin betreten,
aber nur eingeschränkt nutzen.

### Wie viele Urlaubstage habe ich?

Jeder Spieler hat pro Jahr 40 Urlaubstage, diese werden am 1. Januar wieder aufgefüllt.

## Sitting

Anders als im Urlaubsmodus besteht beim Sitting die Möglichkeit, dass der Account weiterhin Rohstoffe produziert.

### Wie aktiviere ich das Sitting?

1. Stelle "Aktiviert" auf Ja
2. Wähle einen beliebigen Loginamen.
3. Wähle ein Passwort
4. Stelle ein bis wann, dass Sitting möglich sein soll.

### Wie loggt sich der Sitter nun ein?

Gebe den gewählten Loginnamen mit den in den Einstellungen stehen Zusätzen ein. Beispiel: _sitter:testaccount:22_
Dazu nun noch das Passwort welches vorher bestimmt wurde.

### Was kann der Sitter machen.

Der Sitter hat zugriff auf deinen gesamten Account, mit Ausnahme von den Einstellungen. Beachtet bei der Benutzung
die [Spielregeln](../legal/rules.md#3-multiusing--sitting--sharing).

## Account löschen
Wenn der eigene Account erfolgreich auf löschen gesetzt wurde, wird man vom System ausgeloggt. Loggt man sich daraufhin
7 Tage nicht ein, wird die Löschung abgebrochen.
