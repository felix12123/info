# Gebäude

## Hauptgebäude

Der Beginn eines jeden Planeten bildet die Kommandozentrale, sie ist der Mittelpunkt der Organisation des Planeten.
Durch den Ausbau dieses Gebäudetyps können andere Gebäude schneller errichtet werden.

Einen ähnlichen Effekt, allerdings für Forschungen, hat das Forschungszentrum. Durch den Ausbau kann auf dem Planeten
schneller [geforscht](resource.md) werden.

## Rohstoffgebäude

Die Rohstoffgebäude teilen sich in 2 Kategorien auf, zum einen die produzierenden Rohstoffgebäude und zum anderen die
Lager, welche darüber entschieden, wie viele Rohstoffe eingelagert werden können und wie hoch
die [Plündergrenze](resource.md#plndergrenze) ist.

### Produktion

Eisenmine und die Lutinumraffinerie, sowie der Bohrturm fördern ihren jeweiligen Rohstoff.
Die Chemiefabrik und die erweiterte Chemiefabrik hingegen fördern Wasserstoff und verbrauchen dabei Wasser.

## Angriff / Verteidigung

In der Schiffsfabrik können nach entsprechender Forschung Schiffe gebaut werden. In der Orbitalen Verteidigungsstation
hingegen werden Türme für die Verteidigung angefertigt.

Mit dem planetaren Schild kannst du den Basis-Verteidigungswert deines Planeten verbessern. Der Fusionsreaktor hingegen
erhöht den Basis-Angriffs- und Basis-Verteidigungswert des Planeten. Damit hat der Fusionsreaktor bei gleichen kosten
ein besseres Kosten-Nutzen-Verhältnis.
Gerade im Kampf gegen Spionagesonden Flotte und kleinere Farmflüge kann diese Kombination sehr effektiv werden.

