# Übersicht
Die Übersicht bildet das Epizentrum von Gigrawars. Hier hast du die Kontrolle über alle Aspekte deiner Flotten und Planetenressourcen.

Auf dieser Seite findest du eine Liste all deiner Flottenbewegungen. Eigene Flotten, sei es für Angriffe, Transporte, Stationierungen oder Kolonisierungen. Ebenso hast du hier die Möglichkeit, fremde Flotten - ob Freund oder Feind - im Auge zu behalten. Besonders wichtige Flottenbewegungen kannst du als "wichtig" über das Sternchen markieren, sodass diese Flottenbewegungen als Erstes gelistet werden und stets präsent sind.

Deine Planetenressourcen sind ebenfalls auf einen Blick ersichtlich. Du kannst verfolgen, wie sich deine Ressourcenbestände entwickeln.

Von der Übersicht gelangst du in die verschiedenen Navigationsreiter.