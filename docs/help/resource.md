# Rohstoffe

## Plündergrenze

Auf der Rohstoffseite befindet sich eine Anzeige mit der Information, wie hoch die aktuelle Plündergrenze deiner Lager
ist.

Nur Rohstoffe über dieser Grenze können geplündert werden. Das heißt, wenn sich 15.000 Eisen im Lager befinden und die
Plündergrenze 3.000 beträgt, dann kann ein Angreifer maximal 12.000 Eisen erbeuten.
