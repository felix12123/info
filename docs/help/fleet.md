# Flotten

## Aufträge

### Angriff

Beim Auftrag Angriff, können feindliche Planten angeflogen werden, mit dem Ziel diese zu Plündern und die Einheiten vor
Ort zu vernichten. Sollte der Zielplanet besiedelt sein, so wird es immer zu einem Kampf kommen, solange es nicht ein
eigener Planet ist.
Für jeden gestarteten Angriff wird ein Punkt des [Angriffslimits](#angriffslimit) verbraucht.

### Erkundung

Bei der Erkundung können im Gegensatz zum Angriff nur Spionagesonden verschickt werden. Die Sonden werden unter allen
Umständen beim Einschlag am Zielplaneten zerstört.
Dafür wird das [Angriffslimit](#angriffslimit) nicht beeinflusst.

::: danger Achtung!

Egal wie groß die Anzahl der Sonden ist, die auf Erkundung geschickt werden. Es werden immer alle Sonden am Ziel
zerstört.
:::

### Stationieren

Der Flottenbefehl Stationieren ermöglicht es, seine Flotte auf einem anderen eigenen Planeten zu stationieren. Eine
Stationierung bei anderen Mitspielern ist nicht möglich.
Anders als oft erwartet, wird bei der Stationierung die volle Menge an Wasserstoff als Treibstoff mitgenommen, da die
Flotte die Chance haben muss, zum Ausgangsplaneten zurückzufliegen, falls sie auf dem Zielplaneten nicht stationiert
werden kann.

Bei diesem Flottenbefehl können Rohstoffe mitgenommen werden.

### Transport

Beim Transport können Rohstoffe auf einem anderen Planeten abgeladen werden, ohne dass die Flotte dort stationiert wird.
Der Transport zu einem fremden Planeten ist möglich.

### Kolonisieren

Für den Flottenbefehl Kolonisieren wird mindestens ein Kolonisationsschiff benötigt. Dieses wird bei Ankunft am Ziel in
eine Kommandozentrale umgewandelt.
Sollten weitere Kolonisationsschiffe mitgeschickt werden, so werden diese nicht auch umgewandelt.

::: tip Rohstoff-Mitnahme

Es empfiehlt sich immer bei einer Kolonisierung Rohstoffe mitzunehmen. Hier ist allerdings zu beachten, dass ein neuer
Planet nur eine gewisse Menge an Rohstoffen fassen kann, außerdem kann es zu [Sondenraids](units.md#sondenraids)
kommen!
:::

## Zurückziehen

Es gibt keinerlei Möglichkeit einen ausgeführten Flottenbefehl zurückzuziehen. Einmal ausgesprochen kann er nicht mehr
zurückgenommen werden.

## Angriffslimit

Das Angriffslimit beträgt 14.000 Flüge pro Woche, diese werden immer in der Nacht von Sonntag auf Montag zurückgesetzt.

## Timing von Flotten

Über die Geschwindigkeit der Flotte können die Ankunftszeiten beeinflusst werden. Die Geschwindigkeit ist allerdings nur
in 10er-Schritten einstellbar.

## Plünderreihenfolge

Mit der Plünderreihenfolge kann gesteuert werden, in welcher Reihenfolge die Laderäume der Flotte gefüllt werden soll,
wenn diese einen Kampf gewinnt.
