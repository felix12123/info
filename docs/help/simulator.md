# Kampfsimulator

Der Kampfsimulator in Gigrawars bietet die Möglichkeit seine Gewinnchance bei einem Angriff / Verteidigung (Att / Deff)
auszurechnen. Sowohl die eigenen Schiffe / Türme als auch die gegnerischen Schiffe / Türme lassen sich hierbei samt der
Forschung eingeben. Die Gewinnchance entscheidet über einen erfolgreichen Kampf sowie die vorhersehbaren Verluste.

Der Kampfsimulator hilft auch dabei möglichst effiziente Flottenkonstellationen (Kombos) zu errechnen, dies ist
insbesondere nützlich, wenn es eine CKK-Grenze in einem Wertungskrieg gibt. Weiterhin kann die benötigte Menge Sonden
für einen erfolgreichen Spionagebericht ermittelt werden als auch die Menge an recycelbaren Rohstoffen ausgegeben
werden.

## Basiswerte

Jeder Planet besitzt eine minimalen Angriffs- und Verteidigungswert. Dieser ist gerade in der Anfangsphase eines
Universums nicht zu unterschätzen.
