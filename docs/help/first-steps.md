# Erste Schritte

## Meine ersten Gebäude

Aller Anfang ist schwer und gerade GigraWars ist zu Beginn etwas ruhiger als vergleichbare Spiele. Die ersten 1-2 Tage
bestehen nur darin zu warten, dass die ersten Gebäude fertig werden. In der Zeit kann man sich dann allerdings auch in
Ruhe mit dem Interface vertraut machen, durch die Galaxie klicken und den Technikbaum studieren.

Zu Beginn sollte man wissen, dass der Rohstoff Eisen wohl für die Anfangszeit der wichtigste Rohstoff ist. Aus diesem
Grund solltest du die Eisenmine erst einmal ein paar Stufen ausbauen, sagen wir mal etwa bis Stufe 5, bevor du überhaupt
daran denkst, etwas anderes zu bauen.

Ansonsten gilt es die Lutinumproduktion anzukurbeln und damit anfangen, die Lutinumraffinerie langsam auszubauen.
Wasserstoff ist ein weiterer Rohstoff, welcher allerdings in den ersten Tagen noch keine große Rolle spielt. Um bald mit
der Produktion der ersten Schiffe und Verteidigungsanlagen zu beginnen, wirst du allerdings Wasserstoff brauchen, nur
eben nicht viel. Baue hierzu vielleicht Bohrturm und deine Chemiefabrik jeweils auf Stufe 1 oder 2 aus. Sie produzieren
dann zwar noch nicht viel, aber bis du so weit bist, dass du den Wasserstoff benötigst, sollte sich etwas was
angesammelt haben.

Bevor du allerdings mit der großen Schiffsproduktion durchstarten kannst, musst du erst einmal dein Forschungszentrum
ausbauen, auch hier reicht Stufe 1 oder 2 für den Anfang. Im späteren Verlauf, wenn du viele Rohstoffe übrig hast,
kannst du mit höheren Stufen die Forschungszeit etwas drücken.

## Meine ersten Forschungen

Du hast also schon ein Forschungszentrum? Dann geht es jetzt weiter mit den Forschungen. Vielleicht hast du gerade ein
paar Rohstoffe übrig und denkst dir, ich forsche einfach mal so darauf los. Kann man so machen, aber ist vielleicht
nicht sonderlich zielführend. Zu Beginn geht es vorrangig darum, die ersten Schiffe und Türme freizuschalten.

Eine Übersicht findest du im Technikbaum, im Menüpunkt **Technik** im Spiel.

Spionagesonden beispielsweise kannst du komplett ohne Forschungen bauen, diese sind allerdings nicht wirklich für
Angriffe gedacht. Deswegen konzentrierst du dich am besten darauf, den Verbrennungsantrieb auf Stufe2 und darauffolgend
den Ionenantrieb auch auf Stufe2 zu erforschen. Jetzt hast du schon eine ganze Reihe von Schiffen freigeschaltet und
brauchst nur noch eine Schifffabrik, um mit dem Bau deiner ersten Schiffe zu starten.

## Meine ersten Schiffe

Sobald du deine erste Schiffsfabrik gebaut hast, wirst du merken, dass du bereits Spionagesonden bauen kannst. Mit
Sonden kannst du die Planeten von Mitspielern ausspionieren. Das klappt vielleicht auch mit einer Sonde, allerdings
steigt die Wahrscheinlichkeit, wenn du mehr Sonden auf einmal nimmst. Hier ist allerdings zu beachten, dass deine Sonden
Schwierigkeiten haben einen Bericht zu erstellen, wenn auf dem Planeten des Gegners Schiffe stehen, da hilft nur warten
oder mehr Sonden zu schicke.

Hast du bereits deine ersten Forschungsstufen erreicht, so wirst du relativ schnell Zugriff auf Kriegsschiffe wie Renegarde
oder Raider haben, falls nicht, solltest du zumindest schon Schakale bauen können. Gegen große Spieler kannst du damit
zwar nicht viel ausrichten, allerdings aber gegen andere kleine oder inaktive Spieler ohne Flotte. Bedenke aber immer,
jeder Planet in GigraWars verfügt über eine [Grundverteidigung](simulator.md#basiswerte), also teste am besten vorher
im [Simulator](simulator.md), wie deine
Chancen stehen.

Viele Spieler bauen am Anfang eine kleine Flotte aus 5 Renegarde oder 5 Raider und zusätzlich, für ihre ersten
Farmgänge, noch ein kleines Handelsschiff. Deswegen kommen wir nun zu den Handelsschiffen, deren Name vielleicht etwas
irreführend erscheinen könnte. Dieses Schiff kannst du nicht nur für den Transport von Rohstoffen einsetzen, sondern
auch im Kampf, um deinen Verteidigungswert aufzubessern oder um mehr Laderaum beim Plündern zu haben.

## Meine erste Kolonie

Es ist so weit, du hast wohl dein erstes Kolonisationsschiff in Auftrag gegeben oder möchtest dich zumindest schon
einmal informieren, wie es weitergeht. In GigraWars kannst du bis zu 20 Planeten besiedeln. Somit ist die Wahl der
Planeten entscheidend für deinen weiteren Spielverlauf. Natürlich kannst du zu jeder Zeit einzelne Planeten wieder
löschen, trotzdem solltest du dir aber überlegen, wie du insgesamt beim Besiedeln vorgehen möchtest.

Es empfiehlt sich, die Planeten nicht alle in ein Sonnensystem zu setzen, sondern etwas über mehrere Galaxien zu
verteilen. Setze daher, deinen ersten Planeten ruhig einige Systeme von deinem aktuellen Planeten entfernt. So weit,
dass du ihn selbst noch gut erreichen kannst, aber trotzdem weit genug weg, um dir neue Farmgründe zu erschließen. Nimm
gerne schon ein paar Rohstoffe beim Kolonisieren mit, um deine Gebäude schneller auszubauen.

## Inaktive erkennen und farmen

Viele Spieler haben sich über die Jahre Methoden überlegt, um inaktive Spieler zu finden. Die wohl einfachste Methode
ist es einen Spieler anzuschreiben und etwas zu fragen, nur musst du damit rechnen, dass er dir bewusst nicht antwortet
oder es vielleicht übersieht. Deswegen bietet diese sowie alle weiteren Methoden keine Garantie, dass ein Spieler
wirklich inaktiv ist.

Alternativ kann man sich zu einem Spieler auf die Punkte merken, mach der Spieler 1-2 Tage keine Punkte so könnte dies
heißen, dass er inaktiv ist. Bei 3-4 Tagen kann man schon relativ sicher sein, allerdings eine absolute Sicherheit gibt
es auch hier nicht. Auch dann nicht, wenn er seine Allianzmitgliedschaft verloren hat.

Eine weitere Möglichkeit wäre, den Spieler zu sonden und sich seine Rohstoffspeicher anzuschauen. Laufen diese über, war
der Spieler schon länger nicht aktiv, sind sie nur knapp über die Plünderungsgrenze gefüllt, wird der Spieler
wahrscheinlich schon aktiv gefarmt. Auch hier kann man sich allerdings nicht ganz sicher sein.

### Farmlisten
Es empfhielt sich die [Targetlisten](targetlist.md) zu nutzen, um Farmlisten für Inaktive zu pflegen.
