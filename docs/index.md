---
home: true
heroText: GigraWars Infos
tagline: Planlos im Weltraum? Gemeinsam werden wir schlauer!
actionText: Hilfe
actionLink: /help/intro
subActionText: Discord
subActionLink: /more/discord
features:
- title: Hilfe
  details: In der Hilfe findest du Informationen zu allen spielrelevanten Seiten.
- title: Community
  details: Auf unserem Discord findest du Gleichgesinnte und unsere Neulingshelfer. 
- title: Tutorials
  details: In Tutorials werden gezielt einzelne Punkte des Spiels erklärt.
---
