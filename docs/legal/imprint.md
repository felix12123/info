# Impressum 

DasDomainDepot.de GmbH  
Feldstraße 41,  
42477 Radevormwald

## Verantwortliche
Magnus Reiß  
Alexander Reiß

## Datenschutzbeauftragter
Alexander Reiß  
[zur Datenschutzerklärung](privacy.md)

## Kontakt
info@gigrawars.de (Kein Support)  
[zum Support](../more/support.md)

## Registereintrag
Eintragung im Handelsregister.  
Registergericht: Köln  
Registernummer: 85943

Umsatzsteuer-Identifikationsnummer gemäß §27 a Umsatzsteuergesetz:  
DE303265692
