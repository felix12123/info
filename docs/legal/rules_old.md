# Spielregeln (Alt)

::: tip Geltungsbereich
Diese Spielregeln gelten für das aktuell laufende Universum "Tailwind".
:::

## §1 Eigentumsverhältnisse
_Unter Eigentum versteht man Rohstoffe, Schiffe oder ähnliche virtuelle Gegenstände, wie der Spielzugang im Zusammenhang mit GigraWars._

**§1 Absatz 1:** Alle im Spiel GigraWars vorhandenen virtuellen Objekte sind Eigentum von GigraWars. Diese werden nur leihweise dem Spieler zur Verfügung gestellt.

**§1 Absatz 2:** Der Verkauf oder Erwerb von virtuellen Objekten, wie Rohstoffe oder Accounts, ist verboten.

## §2 Ingame-Daten
_Zu diesen zählt das interne Spielernachrichtensystem sowie zugehörige Kommunikationskanäle, aber auch veränderbare Namensträger wie Benutzernamen, Planeten, Allianzseiten usw._

**§2 Absatz 1:** In diesen genannten Bereichen ist ein höflicher Umgangston einzuhalten. Beleidigungen und Drohungen sind absolut verboten. Es ist nicht gestattet menschenfeindliches, sexistisches, rassistisches, rechtsverletzendes oder politisch-feindliches Gedankengut zu verbreiten.

**§2 Absatz 2:** Es ist nicht erlaubt über einen Link auf der Allianzseite auf andere Spiele oder Seiten mit nicht GigraWars-bezogenem Inhalt zu verweisen.

**§2 Absatz 3:** Es ist auch nicht gestattet Werbung für Tools auf der Allianzseite oder per Ingame Nachrichten zu versenden, welche gegen (§7) verstoßen.

**§2 Absatz 4:** Bilder und Texte, welche urheberrechtlich geschützt sind, dürfen auf Seiten von GigraWars nicht eingebunden werden.
Der Absatz 5 trifft nicht auf private Nachrichten im Spiel oder im Forum zu!

**§2 Absatz 5:** Allgemein ist es nicht erlaubt, Koordinaten und Kampfberichte in GigraWars zu teilen, außer sie wurden mit dem offiziellem Kampfbericht Tool anonymisiert (zum Tool). Dies gilt auch für Private Daten von Dritten.

## §3 Multiusing / Sitting / Sharing
_Bei Multiusing handelt es sich um das Spielen von mehr als einem Account pro Universum._

**§3 Absatz 1:** Jeder Spieler darf pro Universum nur einen Account erstellen. Das Spielen und/oder Besitzen von mehreren Accounts pro Universum ist verboten. Eine Ausnahme davon ist (§3 Absatz 3).

**§3 Absatz 2:** Das mehrfache Benutzen derselben E-Mail-Adresse, in mehreren Accounts, im selben Universum ist nicht erlaubt.

**§3 Absatz 3:** Ein Spieler darf maximal einen weiteren Account sitten. Während des Sittings sind ausschließlich Maßnahmen zur Sicherung von Spielobjekten bei Gefahr zulässig. Das Ausbauen von Gebäuden, weiterentwickeln von Forschungen, Angriffe auf andere Spieler oder jegliche profitmotivierte Flottenbewegung ist verboten. Ausschließlich das Bewegen der Flotte zu einer anderen Koordinate bei einem Angriffsfall ist zulässig. Wenn zum Schutz eines gesitteten Accounts das Starten eines Bauauftrages notwendig ist, um eine maximale Menge an Rohstoffen zu sichern, so muss dieser unbedingt wieder abgebrochen werden.

**§3 Absatz 4:** Sollten mehrere Personen über eine gemeinsame IP-Adresse bzw. gemeinsamen Internetanschluss spielen, wie in etwa einer Wohnung, einem Firmennetzwerk oder auch öffentliche Netzwerke wie in Universitäten, so ist das im Ticketsystem zu melden. Des Weiteren ist jeglicher Flottenkontakt zwischen diesen Accounts verboten.

**§3 Absatz 5:** Beim Austausch von Ressourcen oder fliegen von Angriffen dürfen diese Accounts nach dem Austausch/Angriff für eine Dauer von 7 Tagen nicht dieselbe IP-Adresse nutzen.

**§3 Absatz 6:** Das Spielen eines Accounts mit mehreren Personen, auch Sharing genannt, ist verboten.

## §4 Urlaubmodus
_Die Regeln zum Urlaubsmodus wurden entfernt, da dieser durch Spiel eigene Systeme geregelt wird._

## §5 Bugusing
_Bugusing bedeutet das absichtliche ausnutzen von Spielfehlern. Bekannte Beispiele sind Flottenverdopplung und Ressourcenverdopplung._

**§5 Absatz 1:** Das Nutzen von bekannten Bugs ist verboten und wird geahndet. Durch Bugs entstehende Vorteile sind sofort dem Support zu melden.

**§5 Absatz 2:** Das Vervielfältigen von virtuellen Objekten, wie Rohstoffe oder Schiffe, ist zu melden. Sonst gilt dies als Bugusing.

**§5 Absatz 3:** Weitere Spielbugs sind ebenfalls über das Ticketsystem zu melden.

**§5 Absatz 4:** Das Melden von Bugs, welche wissentlich nicht existieren oder andere falsche Tatsachen sind verboten.

## §6 Handel / Pushen
_Das Bereitstellen von Rohstoffen an einen anderen Spieler ohne nennenswerte Gegenleistung wird als Pushen bezeichnet._

**§6 Absatz 1:** Das Pushen innerhalb der Allianz ist ausschließlich mit dem Rohstoff Wasserstoff erlaubt, dabei darf das Vorankommen des eigenen Accounts nicht vernachlässigt werden.

_Eine Allianz kann aus mehreren Teilen bestehen, dies muss für jeden Spieler außerhalb der Allianz klar ersichtlich sein, sowie für die Spielleitung.
Als Beispiel wäre hier ein Wing und/oder eine Akademie zu nennen. Ein Fan Club wiederum nicht._

**§6 Absatz 2:** Außerhalb der Allianz ist ein Handel in realistischen Kursen durchzuführen. Ein realistischer Kurs sollte dabei die Menge 1:3 nicht überschreiten, außerdem muss dieser Handel in einem Zeitraum von 72 Stunden abgeschlossen werden. Der Rohstoff Wasser ist beim Handeln ausgeschlossen.

_Der Handel mit Informationen (z.B. Koordinaten gegen Rohstoffe) muss vom Support erlaubt werden. Bei einem Handel dürfen nicht die gleichen Rohstoffe gehandelt werden._

**§6 Absatz 3:**
Beim Neulingspush beziehen sich die Menge immer auf den Spieler, der gepusht wird. Der Neulings-Absatz wird nicht durch den §6 Absatz 1 beschränkt. Die Anzahl der Tage bezieht sich auf die einfache Geschwindigkeit, bei höherer Geschwindigkeit wird die Anzahl der Tage durch die Geschwindigkeit geteilt und aufgerundet.

**§6 Absatz 3a:**
Nach den ersten 45 Tagen eines Universums startet die Neulingshilfe, ab dann ist es generell erlaubt neuen Spielern den Start ins Universum zu erleichtern. Jeder Neuling (welcher noch nie einen zweiten Planeten hatte) darf sich mit bis zu 40.000 Eisen, 40.000 Lutinum und 15.000 Wasserstoff helfen lassen.

**§6 Absatz 3b:**
Nach den ersten 90 Tagen eines Universums, darf Neulingen (welche noch nie einen zweiten Planeten hatten) mit 500.000 Eisen und 500.000 Lutinum geholfen werden. Die Menge an Wasserstoff und Wasser wird dabei nicht begrenzt.

**§6 Absatz 3c:**
Nach den ersten 180 Tagen eines Universums, darf Neulingen (welche noch nie mehr als 5 Planeten hatten) mit 1.500.000 Eisen und 700.000 Lutinum geholfen werden. Die Menge an Wasserstoff und Wasser wird dabei nicht begrenzt.

**§6 Absatz 4:** Das Aussetzen eines Kopfgeldes oder das Durchführen einer Wette, so wie andere vergleichbare Dinge, müssen vom Support genehmigt werden.

## §7 Eingriff in die Spieltechnik
_Darunter versteht man das Abrufen von Spieldaten über ein anderes Programm als den Browser oder das Abrufen von Daten in großen Mengen._

**§7 Absatz 1:** Maßnahmen, welche dazu gedacht sind den Server auszulasten, um den Spielverlauf zu beeinträchtigen, sind verboten.

**§7 Absatz 2:** Das Benutzen von Software oder Skripten, welche in irgendeiner Weise auf das Spielgeschehen zugreifen oder zum ungemeldeten Auslesen von jeglichen Informationen verwendet werden, sind verboten.

**§7 Absatz 3:** Das Abrufen der Seiten von GigraWars mit einem anderen Programm als dem Browser, ist strengstens verboten. Dabei handelt es sich meist um Bots oder andere Tools, welche das Webinterface ersetzen sollen.

**§7 Absatz 4:** Der Zugriff auf die Seiten von GigraWars ist nur mit jeglichen öffentlich zugänglichen Browsern erlaubt (Beispiel: Google Chrome, Mozilla Firefox, Opera, Microsoft Edge, Apple Safari). Nicht erlaubt sind Erweiterungen und Plugins welche die Seiten von GigraWars verändern.

**§7 Absatz 5:** Das Verändern von Links die von GigraWars stammen, ist händisch sowie mit einem Programm verboten.

**§7 Absatz 6:** Das Zugreifen auf einen fremden Account ohne Zustimmung des Besitzers ist strengstens verboten.

## §8 Angriffe / Spamangriffe
_Unter Spamflotten versteht man Flotten, die nur das Ziel der Blockierung des Gegners mit unlauteren Mitteln haben. Darunter fallen zum Beispiel Angriffe, die nur geflogen werden, um die Übersicht des Gegners möglichst lange und mit vielen Angriffsmeldungen zu blockieren._

**§8 Absatz 1:** Flotten, die länger als 24h bis zum Gegner fliegen, sind verboten.

_Zum Zählen der gleichzeitigen Flotten auf einen Spielaccount kann man im eigenen Account auf der Übersicht alle Flotten zählen, die derzeit auf dem Weg zum gleichen Spielaccount sind._

**§8 Absatz 2:** Es ist verboten, mehr als 75 Flotten insgesamt gleichzeitig auf einen Spielaccount zu schicken.

## §9 Kriege
_Solange es keine Veröffentlichung des Diplomatiesystems gibt, sind Kriege ausschließlich im Discord anzumelden. Über das Diplomatiesystem wird ein Krieg sofort gültig, sobald beide Parteien diesen akzeptieren._

**§9 Absatz 1:** Ein Krieg ist gültig, sobald beide Parteien diesem zugestimmt haben.

## §10 Schadensersatz
_Hierbei geht es um Verluste von virtuellen Objekte, welche das Spiel betreffen._

**§10 Absatz 1:** Bei Administrations-, Programmier- und Datenfehlern, besteht kein Anspruch auf Entschädigung.

**§10 Absatz 2:** Sollte einem Spieler durch einen als Multi/Cheater/Scripter/Buguser enttarnten Spieler Schaden entstehen oder entstanden sein, gibt es keine Wiedergutmachung.

**§10 Absatz 3:** Schäden welche durch eine unrechtmäßige Sperre entstehen sind, werden nicht ersetzt.

**§10 Absatz 4:** Die Spielleitung behält sich das Recht vor, in Einzelfällen Verluste, welche durch Spielfehler entstanden sind, zu ersetzen.

## §11 Zusatzregeln
**§11 Absatz 1:** Es ist verboten sich als GigraWars Administrator auszugeben. Darunter fallen beispielsweise das Erschleichen von Passwörtern oder das Vortäuschen eines Administrator-Accounts und des damit verbundenen Angriffsverbotes in Allianztexten oder Nachrichten.

**§11 Absatz 2:** Das Angreifen von Administratoren ist verboten, außer bei Absprache mit diesen.

**§11 Absatz 3:** Durch die Anmeldung bei GigraWars und das Einloggen nach Erhalten des Passworts bestätigt der Spieler automatisch die Teilnahme am sogenannten Newsletter.

_Hier geht es um das Versenden des Newsletters zum Start einer neuen Runde, welcher nur gelegentlich vollzogen wird._

**§11 Absatz 4:** Die Regeln können jederzeit durch die Spielleitung geändert werden. Dies wird in den News bekannt gegeben.

**§11 Absatz 5:** Fälle die nicht eindeutig durch die Regeln abgedeckt sind, werden von der Spielleitung entschieden.

*Stand 03.10.2023*