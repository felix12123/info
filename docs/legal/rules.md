# Spielregeln

::: tip Info
Bist du auf der Suche nach den Spielregeln für das laufende Universum Tailwind, dann findest du die Spielregeln [hier](rules_old.md).

:::

Diese Spielregeln gelten verbindlich für alle Universen von GigraWars.

Ihr Zweck besteht darin, sicherzustellen, dass allen Spielern ein gerechtes Spielerlebnis gewährleistet wird und den Spielspaß in den Universen fördert.

Es ist zu beachten, dass Anfragen bezüglich eines Accounts aus Datenschutzgründen ausschließlich mit dem Kontobesitzer besprochen werden können.

Die Spielregeln gelten in Kombination mit den [AGB](agb.md).

## §1 Ingame-Daten
_Zu diesen zählt das interne Spielernachrichtensystem sowie zugehörige Kommunikationskanäle, aber auch veränderbare Namensträger wie Benutzernamen, Planeten, Allianzseiten usw._

**§1 Absatz 1:** Es ist nicht erlaubt, über einen Link auf der Allianzseite auf andere Spiele oder Seiten mit nicht GigraWars-bezogenem Inhalt zu verweisen.

**§1 Absatz 2:** Es ist auch nicht gestattet, Werbung für Tools auf der Allianzseite oder per Ingame Nachrichten zu versenden, welche gegen die AGB verstoßen.

**§1 Absatz 3:** Allgemein ist es nicht erlaubt, öffentlich Koordinaten und Kampfberichte in GigraWars zu teilen, außer sie wurden mit dem offiziellen Kampfbericht-Tool anonymisiert (zum Tool). Dies gilt auch für private Daten von Dritten.

## §2 Multiusing / Sitting / Sharing
_Bei Multiusing handelt es sich um das Spielen von mehr als einem Account pro Universum._

**§2 Absatz 1:** Jeder Spieler darf pro Universum nur einen Account erstellen. Das Spielen und/oder Besitzen von mehreren Accounts pro Universum ist verboten.

**§2 Absatz 2:** Sollten mehrere Personen über eine gemeinsame IP-Adresse bzw. gemeinsamen Internetanschluss spielen, wie in etwa einer Wohnung, einem Firmennetzwerk oder auch öffentliche Netzwerke wie in Universitäten, so ist das im Support zu melden. Des Weiteren ist jeglicher Flottenkontakt zwischen diesen Accounts verboten.

**§2 Absatz 3:** Beim Austausch von Ressourcen oder dem Fliegen von Angriffen dürfen diese Accounts vor und nach dem Austausch/Angriff für eine Dauer von 7 Tagen nicht dasselbe Netzwerk nutzen.

**§2 Absatz 4:** Das Spielen eines Accounts mit mehreren Personen, auch Sharing genannt, ist verboten.
## §3 Kriege
_Solange es keine Veröffentlichung des Diplomatiesystems gibt, sind Kriege ausschließlich im Discord anzumelden. Über das Diplomatiesystem wird ein Krieg sofort gültig, sobald beide Parteien diesen akzeptieren._

**§3 Absatz 1:** Ein Krieg ist gültig, sobald beide Parteien diesem zugestimmt haben.
## §4 Zusatzregeln

**§4 Absatz 1:** Es ist verboten, sich als GigraWars Administrator auszugeben. Darunter fallen etwa das Erschleichen von Passwörtern oder das Vortäuschen eines Administrator-Accounts und des damit verbundenen Angriffsverbotes in Allianztexten oder Nachrichten.

**§4 Absatz 2:** Sollten Bestimmungen dieser Spielregeln unwirksam sein oder werden, so berührt dies die Wirksamkeit der übrigen Bestimmungen nicht.

**§4 Absatz 3:** Fälle, die nicht eindeutig durch die Regeln abgedeckt sind, werden von der Spielleitung entschieden.
